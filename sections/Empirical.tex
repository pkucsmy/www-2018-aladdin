Given the benefits of deep links for mobile apps, in this section, we present an empirical study to understand the state of practice of deep links in current Android apps. We focus on three aspects: \textit{(1) the trend of deep links with version evolution of apps}; \textit{(2) the number of deep links in popular apps}; (3) \textit{how deep links are realized in current Android apps.}
% and Wandoujia\footnote{Wandoujia is the top Android app store in China, owning more than 300 million users and 1 million free free apps}~\cite{Web:Wandoujialeading}.
% as well as 8 popular open-source apps from GitHub.

%\subsection{Usage Scenarios of Deep Links}
%\begin{figure}[t]
%\centering
%  \includegraphics[width=0.8\columnwidth]{figures/android-app-indexing}
%  \caption{Google App Indexing.}~\label{fig:example}
%\end{figure}

%\textbf{App Indexing}. The goal of app indexing is to put the apps in front of users who use a search engine. When a search result can be served from an app, users who have installed the app can go directly to the page containing the result. Figure~\ref{fig:example} shows an example of Google App Indexing. When a user searches a term ``\textit{triple chocolate therapy recipe}'' on Google, one result comes from the `\textit{`Allthecooks Recipes}'' app. If the user clicks the button ``Open in app'', then the target app page of the search result is directly opened. App Indexing is actually a concrete usage scenario of deep link.

%\textbf{Sharing}. Use Facebook App Links as an example.

%\textbf{Bookmarks}. Use iPhone search box as an example.

%\subsection{Issues of Current Deep Links}
In practice, there is no standard way of measuring how many deep links an app has. However, according to Android's developer guide~\cite{AndroidGuide}, we can infer an essential condition, i.e., \textit{\textbf{activities that support deep links MUST have a special kind of intent filters declared in the \texttt{AndroidManifest.xml} file}}. Such a kind of intent filters should use the \texttt{android.intent.}\texttt{action.VIEW} with the \texttt{android.}\texttt{intent.category.BROWSABLE} category. We denote these intent filters as deep-link related. If an activity has deep-link related intent filters, then we say the activity is registered with deep links. Therefore, we can take the number of activities registered with deep links as an indicator to estimate the number of deep links for an Android app. We should mention that activities without deep-link related intent filters may still register deep links because developers can register a single activity with deep links and forward the link request to other activities. As a result, our estimation of deep links may be below the actual number. However, such a case usually exists in apps that have only one activity registered with deep links. So our results are still able to reveal the status of deep links in practice.
\vspace{-1em}
\subsection{Evolution of Deep Links with Versions}
We first validate that the support of deep links is really desired. To this end, we investigate the trend of deep links along with the version evolution. We choose top 200 apps (as of Jan. 2017) ranked by Wandoujia, a leading Android app store in China~\cite{Li:IMC15, Lu:ICSE16}. To make comparison, we manually download each app's first version that could be publicly found on the Internet and its latest version published in Wandoujia as of Jan. 2017. We compare the number of deep links in the two versions of each app. Figure~\ref{fig:motivation:trend1} shows the distribution of the number of deep links among all the apps in each version. Generally, it is observed that when first released, only 30\% of apps have deep links. In contrast, more than 80\% of these apps have supported deep links in their latest versions. More specifically, the maximum number of deep links is 35 in the first version and it increases to 81 in the latest version. Such a change indicates that the popularity of deep links keeps increasing in the past few years.

\subsection{Coverage of Deep Links}\label{sec:empirical}
Although the number of deep-link supported apps increases, the percentage of activities that have deep links in deep-link supported apps is still rather low. We compute the ratio of activities with deep links to the total number of activities. As shown in Figure~\ref{fig:motivation:trend2}. It is surprising to find that the percentage of activities with deep links becomes smaller from the first to the latest version. For the first version, there are more than 20\% of apps of which the percentage of activities with deep links is above 10\%, but the number of apps decreases to 15\% for the latest version. The reason is that developers add more activities when upgrading their apps but they release deep links to only fixed activities.

%We can see that the distribution of the percentage does not change much between the first version and latest version. Among 90\% of deep-link supported apps, only about 5\% of activities have deep links. A small difference is that the maximum percentage of deep-linked activities increases from 50\% in the first version to 85\% in the latest version.

Aiming to expand the investigation to a wide scope, we study the latest version of 20,000 popular apps on Wandoujia, and 5,000 popular apps on Google Play as of Jan. 2017. Figure~\ref{fig:motivation:coverage1} shows the distribution of the number of deep links among apps in the two app markets. Similar to the preceding results, more than 70\% and 90\% of the apps do not have deep links on Wandoujia and Google Play, respectively. Such a result indicates that deep links are not well supported in a large scope of current Android apps, especially the international apps on Google Play.

Considering the percentage of activities with deep links, Figure~\ref{fig:motivation:coverage2} shows that the median percentage is just about 4\%, implying that a very small fraction of the locations inside apps can be actually accessed via deep links. About only 10\% of apps have more than 20\% of activities with deep links. There is no significant difference between the distribution on Wandoujia and Google Play, meaning that the low coverage of deep links is common for the Android ecosystem.

In summary, the preceding empirical study demonstrates the low coverage of deep links in current Android apps. Such a result is a bit out of our expectation, since deep links are widely encouraged in industry to facilitate in-app content access. There are four possible reasons leading to the low coverage of deep links. First, as deep link is a relatively new concept, it may take some time to be adopted by Android developers. Second, due to commercial or security considerations, developers may not be willing to expose their apps to third parties through deep links. Third, the developers do not have clear motivation to determine which part of their apps needs to be exposed by deep links. Fourth, as we show later, implementing deep links requires non-trivial developer efforts so that developers may not be proactive to introduce deep links in their apps. However, deep link is still promising in the mobile ecosystem given the strong advocation by major Internet companies as well as the potential revenue brought by opening data and cooperating with other apps.
\begin{table}[t]
\centering
\small
\caption{LoC changes when adding deep links of open-source apps on GitHub.}\label{table:locgithub}
\vspace{-1em}
\begin{tabular}[t]{l|r}
 \hline
 % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  \textbf{Repository Name} & \textbf{LoC Changes}\\
  \hline
  stm-sdk-android & 45 \\
  mobiledeeplinking-android & 62 \\
  WordPress-Android & 73 \\
  mopub-android-sdk & 78 \\
  ello-android & 87 \\
  bachamada & 179 \\
  sakai & 237 \\
  SafetyPongAndroid & 411 \\
  \hline
\end{tabular}
\vspace{-1.5em}
\end{table}

\subsection{Developer Efforts}
Indeed, supporting deep links requires the developers to write code and implement the processing logics, not just by declaring the intent filter in the AndroidManifest.xml file. %In practice, there are usually two ways of implementing deep links. One is to establish implicit intents for each activity that needs to be equipped with deep links. The other is to use a central activity to handle all the deep links and dispatch each deep link to its target activity.
Although there have already been some SDKs for deep links~\cite{mobiledeeplinking,DeepLinkDispatch}, implementing deep links exactly requires the modifications or even refactoring of the original implementation of the apps. We then study the actual developer efforts when releasing deep links for an Android app.

For simplicity, we study the code evolution history of open-source apps on GitHub. We search on GitHub with the key word ``\textit{deep link}'' among all the code-merging requests in the Java language. There are totally 4,514 results returned. After manually examining all the results, we find 8 projects that actually add deep links in their code-commit history. We carefully check the code changes in the commit related to deep links. Table~\ref{table:locgithub} shows the LoC (lines of code) of changes in each project when adding deep links to just \emph{one} activity in the corresponding code commit. The changes include the addition, modification, and deletion of the code. We can observe that the least number of the LoC change is 45 and the biggest can reach 411. Take the app \texttt{SafetyPongAndroid}\footnote{\url{https://github.com/SafetyMarcus/SafetyPongAndroid}} as an example. We find that a large number of changes attribute to the refactoring of app logics to enable an activity to be directly launched without relying on other activities. Several objects that are previously initialized in other activities need to be initialized in the activity to be deep linked. Such an observation can provide us the preliminary findings that developers need to invest non-trivial manual efforts on existing apps to support deep links. Such a factor could be one of the potential reasons why deep links are of low coverage.

%\subsection{Implications from the Study}
%Current support of deep links is rather limited. There are two issues that should be resolved.
