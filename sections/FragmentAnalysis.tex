As shown in Section~\ref{sec:background}, there are different fragments in an activity for serving as user interface, just like the frames in a web page. In order to reach a specific fragment directly with a deep link, we should further analyze how to transfer to fragments of an activity.

Contrary to activity transitions where intents can be sent to invoke the transition, the fragment transitions often occur after users perform an action on the interface such as clicking a view, then the app gets the user action and executes the transition. Due to the dynamics of activities, fragments of activities may be dynamically generated, just like AJAX on the Web. To the best of our knowledge, it is currently not possible to find out fragments by static analysis. Thus, we tend to use dynamic analysis, traversing the activity by clicking all the views on the page in order to identify all the fragments and their corresponding trigger actions.

%After developers select the activity to implement deep links, a simulator will be launched and build the graph. Aladdin will list all the fragments and paths to developers for them to choose.

\subsubsection{Fragment Identification}
Unlike activities where class name is the identifier of different activities, fragments usually do not have explicit identifiers. To determine whether we have switched the fragment after clicking a view, we use the view structure to identify a certain fragment. In Android, all the views are organized in a hierarchy view tree. We get the view tree at runtime and design Algorithm~\ref{algo:structhash} to calculate the structure hash of this tree, and use the hash to identify the fragments. The algorithm is recursive with a view $r$ as input. If $r$ does not have children, the result is only the string hash of $r$'s view tag (Line 2). If $r$ has children (Line 3), then we use the algorithm to calculate all the hash of its children recursively (Lines 5-7). Then, we sort $r$'s children based on their hash values to ensure the consistency of the structure hash, because a view's children do not keep the same order every time (Line 8). Next, we add each children's hash together with the view tag forming a new string (Line 10), and finally return the string hash (Line 13). When inputing the root view of the tree to the algorithm, we could get a structure hash of the view tree. The hash can be used as an identifier of a fragment.

\begin{algorithm}
\scriptsize
    \caption{Computing structure hash of view tree.}
    \label{algo:structhash}
    \SetAlgoLined
    \KwIn{View $r$}
    \KwOut{Structure Hash $h$}
        \textbf{function}~TreeHash($r$)\\
        $str \leftarrow r.viewTag$\\
        \If{$r.hashChildren()$} {
            $children \leftarrow r.getChildren()$\\
            \ForEach{$c \in children$} {
                $c.hash \leftarrow TreeHash(c)$
            }
            $children \leftarrow sort\_by\_hash(r.getChildren())$\\
            \ForEach{$c \in children$} {
                $str += c.hash$
            }
        }
    \Return{$hash(str)$}
\end{algorithm}
\subsubsection{Fragment Transition Graph}
In order to retrieve all the fragments as well as triggering actions to each fragment, we define a fragment transition graph to represent how fragments are switched in an activity.
\begin{Definition}[Fragment Transition Graph]
A Fragment Transition Graph is a directed graph with a start vertex. It is denoted by a 3-tuple, $FTG <V, E ,r>$. $V$ is the set of vertices, representing all the fragments of an activity, identified by the structure hash. $E$ is the set of directed edges. Each edge e is a 3-tuple, $e <S, T, I>$. $S$ and $T$ are source and target fragments where $I$ represents the resource id of the view which invokes the transition. $r$ is the start vertex.
\end{Definition}

The dynamic analysis performs on an instance of an activity. Therefore, after developers select the activity to support deep links to fragments, a simulator is launched and developers are asked to transfer to an instance page of this activity. From this page, the simulator traverses the activity in the depth-first sequence. For each view in the current fragment, we try to click it and check whether the current state has changed. If the activity has changed, then we can use the system function \texttt{doback()} to directly return to the previous condition. Otherwise, we check the fragment state. If the structure hash of the current fragment is different from that of the previous one, the fragment has changed. Therefore, we can add it into the edge set if it is a new fragment. The dynamic analysis is similar to the web crawlers that need to traverse every web page, except that Android provides only a \texttt{doback()} method that can return to the previous activity, but not to the previous fragment. So to implement backtrace after fragment transitions, we have to restart the app and recover to the previous fragment.
%
%\begin{algorithm}
%%\small
%    \caption{Generation of the Fragment Transition Graph.}
%    \label{algo:FTGGeneration}
%    \SetAlgoLined
%    \KwIn{Activity $a$, Fragment $f$}
%    \KwOut{Fragment Transition Graph $G$}
%        \textbf{function}~FTGBuild($a, f$)\\
%        \ForEach{$v\in f.views()$} {
%            $v.click()$\\
%            \If{$getCurrentActivity()\neq a$} {
%                $doback()$\\
%                \textbf{continue}
%            }
%            $cf\leftarrow getCurrentFragment()$\\
%            \If{TreeHash($cf.root$)$\neq$TreeHash($f.root$)} {
%                \If{$cf \notin G.V$} {
%                    $G.E.add(<f, cf, v>)$\\
%                    $FTGBuild(a, cf)$
%                }
%                $recover()$\\
%               \textbf{continue}
%            }
%        }
%\end{algorithm}

After finishing the traverse search, we can get the fragment transition graph and a list of fragments. To get the action path towards a certain fragment, we simply combine all the edges from the start vertex to the fragment. %Thus, developers can choose any fragment to form a deep link by putting the actions into the action sequence in the deep link template.
