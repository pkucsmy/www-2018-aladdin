In practice, deep links are implemented based on the mechanism of Inter-Process Communication provided by mobile systems. For Android, current deep links are located to activities and implemented based on implicit intents. For activities to be deep linked, developers first add intent filters when declaring activities in the \texttt{AndroidManifest.xml} file. Intent filters specify the URI patterns that the app can handle from inbound links. When an implicit intent is sent out, the Android system launches the corresponding activity whose intent filter can match to the intent, thus realizing the execution of a deep link. The code snippet in Listing~\ref{code:intentfilter} illustrates an example of intent filter for links pointing to \url{http://www.examplepetstore.com}. In order to enable the deep links to be opened within Web browsers, the \texttt{action} element should be \texttt{android.intent.action.VIEW} and the \texttt{category} element should include \texttt{android.intent.} \texttt{category.BROWSABLE}.

\begin{scriptsize}
\begin{lstlisting}[caption={Intent Filter Configuration of Deep Links}, label = {code:intentfilter},
	language=XML]
<activity android:name="com.example.android.PetstoreActivity">
  <intent-filter>
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" />
    <data android:scheme="http" />
    <data android:host="www.examplepetstore.com" />
  </intent-filter>
</activity>
\end{lstlisting}
\end{scriptsize}

After declaring the intent filter, the next step is to add the business logic to handle the intent filters. The code snippet in Listing~\ref{code:intentfilterlogic} shows the logic of handling the intent filter defined previously. Once the Android system starts the activity through an intent filter, the business logic is called to use the data provided by the intent to determine the app's view response. The methods \texttt{getDataString}() can be used to retrieve the data associated with the incoming intent during the launching callbacks of the activity such as \texttt{onCreate}() or \texttt{onNewIntent}().

\begin{scriptsize}
\begin{lstlisting}[caption={The Logic of Handling the Intent Filter}, label = {code:intentfilterlogic}, language=Java]
protected void onNewIntent(Intent intent) {
   String action = intent.getAction();
   String data = intent.getDataString();
   if (Intent.ACTION_VIEW.equals(action) && data != null) {
      String productId;
      productId = data.substring(data.lastIndexOf("/") + 1);
      Uri contentUri = PetstoreContentProvider
                .CONTENT_URI.buildUpon()
                .appendPath(productId).build();
      showItem(contentUri);
   }
}
\end{lstlisting}
\end{scriptsize}

However, in the real-world apps, implementing deep links for activities are much more than declaring an intent filter and adding the logic. Refactoring has to be made on the current implementation of apps. The code snippet in Listing~\ref{code:example} shows an example of a typical Android app. The app has three activities $Main$, $A$, and $B$. $Main$ activity is the entrance of the app. It has two buttons: when $button1$ is clicked, activity $A$ is invoked by an intent with a string parameter $p1$ (intent $i1$); when $button2$ is clicked, a fragment $frag$ is shown on the current $Main$ activity. When activity $A$ is launched, a $fooList$ is initialized with the incoming parameter $p1$. Activity $A$ may invoke activity $B$ with an intent $i2$ to display the detail of a $foo$ when $button3$ is invoked. In the following, we use this example to illustrate the efforts of implementing deep links.

$Main$ activity naturally supports deep link because the intent to launch the app always opens the $Main$ activity. For activity $A$, it is relatively easy to add a deep link because the intent to launch $A$ from $Main$ contains only a string parameter $p1$ that can be directly implemented by an implicit intent. However, for activity $B$, although the intent to launch $B$ from $A$ contains only an integer parameter $foo$, the deep link to $B$ cannot be simply implemented by an implicit intent because $B$ relies on the data structure $fooList$ constructed in $A$ to retrieve the specific instance of $Foo$ by $fooIndex$. Such a kind of data dependency is prevalent for Android apps to achieve better performance. As a result, in order to add a deep link to $B$, developers have to refactor the implementation of $B$ to initialize $fooList$, or to add extra logics to retrieve the requested instance of $Foo$ according to $fooIndex$.

Current Android apps usually use fragments to organize the views of an activity. In the motivating example, $Main$ activity has a fragment $frag$ that is shown only when a button is clicked. Adding deep links to only activities is sometimes too coarse-grained. As a result, it is required to reach the specific fragments within an activity by deep links, just like reaching a specific part of a web page by an anchor in the URL. Different from activity transitions based on standard intents, fragment transitions are usually implemented by internal functions that vary among different apps. When adding a deep link to the fragment, developers have to handle the transition to the fragment $frag$, e.g., in a way of encapsulating the fragment transition into a common function and invoking the function according to incoming parameters. Such code refactoring requires re-designing the fragment-management logic of the activity.

On the whole, as we will show later in the empirical study, adding deep links to Android apps requires non-trivial developer efforts. On one hand, some activities cannot be directly reached due to data dependencies on other activities. On the other hand, the prevalence of fragment usage requires fine-grained deep links that can reach specific fragments inside an activity.

\begin{scriptsize}
\begin{lstlisting}[caption={Motivating Example}, label = {code:example},language=Java]
public class Main extends Activity
                        implements OnClickListener {
   public void onCreate(Bundle savedInstanceState) {
      ...
      button1.setOnClickListener(this);
      button2.setOnClickListener(this);
      ...
   }
   public void onClick(View v) {
      switch (v.getId()) {
      case R.id.button1:
         Intent i1 = new Intent(Main.this, A.class);
         String s1 = ...;
         intent.putExtra("p1", s1);
         startActivity(intent);
         break;
      case R.id.button2:
         Fragment frag = new ChildFragment();
         getFragmentManager().beginTransaction().add(frag);
         break;
      ...
      }
   }
}
public class A extends Activity {
   static List fooList;
   public void onCreate(Bundle savedInstanceState) {
      String s = getIntent().getStringExtra("p1");
      fooList = getFooList(s);
      ...
      button3.setOnClickListener(new OnClickListener() {
         public void onClick(View v) {
            int fooIndex = ...;
            Intent i2 = new Intent(A.this, B.class);
            intent.putExtra("foo", fooIndex);
            startActivity(intent);
         }
      });
   }
}
public class B extends Activity {
   public void onCreate(Bundle savedInstanceState) {
      int fooIndex = getIntent().getIntExtra("foo");
      Foo foo = (Foo) A.fooList.get(fooIndex);
      ...
   }
}
\end{lstlisting}
\end{scriptsize}
