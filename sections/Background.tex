In this section, we present some background knowledge of Android apps and deep links.

\subsection{A Conceptual Analogy}
%\begin{figure}[t]
%\centering
%  \includegraphics[width=0.5\textwidth]{figures/AndroidAct}
%  \caption{Android application model.}~\label{fig:activity}
%\end{figure}
%In Figure~\ref{fig:activity}, we illustrate the structure of a typical Android app from Android developer guide~\cite{AndroidGuide}.
An Android app, identified by its package name, usually consists of multiple \texttt{Activities} that are loosely bound to each other. An activity is a component that provides a user interface that users can interact with, such as dialing phones, watching videos, or reading news. Each activity is assigned a window to draw its graphical user interface. One activity in an app is specified as the ``main'' activity, which is first presented to the user when the app is launched.

For ease of understanding, we can draw an analogy between Android apps and the Web, as compared in Table~\ref{table:concepts}. An Android app can be regarded as a website where the package name of the app is like the domain of the website. Therefore, activities can be regarded as web pages because both of them are basic blocks for apps and websites, respectively, providing user interfaces. The main activity is just like the home page of a website.

An activity has several view components to display its user interface, such as \texttt{TextView, ButtonView, ListView}. View components are similar to web elements consisting of a web page, such as \texttt{<p>}, \texttt{<button>}, and \texttt{<ul>}. When a web page is complex, frames are often used to embed some web elements for better organization. Frames are subpages of a web page. Similarly, since the screen size of mobile devices is rather limited, Android provides \texttt{Fragment} as a portion of user interfaces in an activity. Each fragment forms a subpage of an activity.

Activities and fragments hold the contents inside apps, just like web pages and frames, which encapsulate contents on the Web. In the rest of this paper, we use the term ``page'' and ``activity'' exchangeably, as well as ``subpage'' and ``fragment'' exchangeably, for ease of presentation.

Transitions between activities are executed through \texttt{Intents}. An intent is a messaging object used to request an action from another component, and thus essentially supports Inter-Process Communication (IPC) at the OS level. Note that intents can be used to transit between activities from both the same apps and two different apps. There are two types of intents: (1) \textit{explicit} intents, which specify the target activity by its class name; (2) \textit{implicit} intents, which declare \texttt{action}, \texttt{category}, and/or \texttt{data} that can be handled by another activity. Messages are encapsulated in the \texttt{extra} field of an intent. When an activity $P$ sends out an intent $I$, the Android system finds the target activity $Q$ that can handle $I$, and then loads $Q$, achieving the transition from $P$ to $Q$.

\begin{table}[t]
\centering%\small
\small
\caption{Conceptual comparison between Android apps and the Web.}
\vspace{-1em}
\label{table:concepts}
\begin{tabular}{l|l}
 \hline
  \textbf{Concepts of Android Apps} & \textbf{Concepts of Web}\\
  \hline
  app & website\\
  package name & domain\\
  activity & web page\\
  main activity & home page\\
  view component & web element\\
  fragment & frame\\
  \hline
\end{tabular}
  \vspace{-1.5em}
\end{table}
\subsection{Deep Links}\label{sec:deeplink}
The idea of deep links for mobile apps originates from the prevalence of hyperlinks on the Web. Every single web page has a globally unique identifier, namely URL. In this way, web pages are accessible directly from anywhere by means of URLs. Typically, web users can enter the URL of a web page in the address bar of web browsers, and click the ``Go'' button to open the target web page. They can also click through hyperlinks on one web page to navigate directly to other web pages. %Indeed, the hyperlinks are instrumental to many fundamental user experiences such as navigating from one web page to another, bookmarking a page, or sharing it with others, e.g., like \texttt{del.icio.us}\footnote{Del.icio.us. \url{https://del.icio.us/}}.

%\begin{figure*}
%  \centering
%    \subfigure[Trend of apps with deep links]{
%    \label{fig:motivation:trend1} %% label for second subfigure
%    \includegraphics[width=0.24\textwidth]{figures/deeplink_trend_num}}
%    \subfigure[Trend of deep-linked activities in deep-link supported apps]{
%    \label{fig:motivation:trend2} %% label for second subfigure
%    \includegraphics[width=0.24\textwidth]{figures/deeplink_trend_percentage}}
%    \subfigure[Distribution of the number of deep links]{
%    \label{fig:motivation:coverage1} %% label for first subfigure
%    \includegraphics[width=0.24\textwidth]{figures/link_num}}
%    \subfigure[Distribution of the percentage of deep-linked activities]{
%    \label{fig:motivation:coverage2} %% label for second subfigure
%    \includegraphics[width=0.24\textwidth]{figures/link_percentage}}
%  \caption{The trend and status of deep links among Android apps.}
%  \label{figure:motivation} %% label for entire figure
%\end{figure*}
\begin{figure*}
  \centering
    \subfigure[Trend of apps with deep links]{
    \label{fig:motivation:trend1} %% label for second subfigure
    \includegraphics[width=0.24\textwidth]{figures_new/2-1}}
    \subfigure[Trend of deep-linked activities in deep-link supported apps]{
    \label{fig:motivation:trend2} %% label for second subfigure
    \includegraphics[width=0.24\textwidth]{figures_new/2-2}}
    \subfigure[Distribution of the number of deep links]{
    \label{fig:motivation:coverage1} %% label for first subfigure
    \includegraphics[width=0.24\textwidth]{figures_new/2-3}}
    \subfigure[Distribution of the percentage of deep-linked activities]{
    \label{fig:motivation:coverage2} %% label for second subfigure
    \includegraphics[width=0.24\textwidth]{figures_new/2-4}}
  \vspace{-1em}
  \caption{The trend and status of deep links among Android apps.}
  \vspace{-1em}
  \label{figure:motivation} %% label for entire figure
\end{figure*}

Compared to web pages, the mechanism of hyperlinks is historically missing for mobile apps. %A page of an app has only an internal location. Accessing a page in an app has to be started by launching the app, and users may need to walk through various pages to reach the target one. For example, a user can find a favourite restaurant in a food app such as \texttt{Yelp}. Next time when the user wants to check the restaurant information, she has to launch the app, search the restaurant again, and then reach the page of the restaurant's details. In other words, there is no way for the user to directly open the restaurant page even if she has visited before.
To solve the problem, deep links are proposed to enable directly opening a specific page inside an app from the outside of this app with a uniform resource identifier (URI). %A deep link can launch an already installed app on a user's mobile device (similar to loading the home page of a website) or it can directly open a specific location within the app (similar to deep linking to an arbitrary web page in a website)~\cite{uLink:MobiSys2016}.
The biggest benefit of deep links is not limited in enabling users to directly navigate to specific locations of an app, but further supports other apps or services (e.g., search engines) to be capable of accessing the internal data of an app and thus enables ``communication" of apps to explore more features, user experiences, and even revenues. Below are some usage scenarios of deep links.

\begin{itemize}
\item {\textbf{In-App Search}. In-app search~\cite{GoogleAppIndexing} enables mobile users to search contents inside apps and enter directly into the app page containing the information from search results.}%For example, Google proposes app indexing~\cite{GoogleAppIndexing} to realize in-app search. When a search result can be served from an app, users who have installed the app can go directly to the page containing the result.}
\item {\textbf{Bookmarking}. Mobile users can create bookmarks to the information or functionalities inside apps for later use~\cite{uLink:MobiSys2016}.}
    %For example, a flashlight app has a fantastic effect that its users may frequently use. So the app could provide a deep link to the functionality of the fantastic effect and enable users to create a bookmark on their home screen. Afterwards, users could directly activate the fantastic effect by just clicking a bookmark on the home screen.}
\item {\textbf{Content Sharing}. With deep links, mobile users can share pages from one app to friends in social networking apps~\cite{FacebookAppLinks}.}% For example, Facebook designs app links~\cite{FacebookAppLinks} as an open standard for deep links to contents in third-party apps.}
\item {\textbf{App Mashup}. Other than simple content sharing, deep links can further act as the support for realizing ``\textit{app mashup}''~\cite{Ma:ICWS2015} to integrate services from different apps, like IFTTT~\cite{IFTTT}.} %For example, a user books a hotel and views restaurant using \texttt{Expedia} and \texttt{Yelp}, respectively. Suppose a deep link can share the location information of hotels from \texttt{Expedia} to map the information of restaurants in \texttt{Yelp}. Then, it is possible to automatically create a trip itinerary by mashing up hotel and restaurant information that the user viewed and interacted with, through clicks on the deep link with system support such as Appstract~\cite{Suman:Mobicom16}. In fact, it is also observed that the ``Now on Tap'' service~\cite{nowontap} from recently released Google Pixel smartphone has leveraged deep links and app indexing to realize such app-mashup user experiences.
\end{itemize}
%\begin{figure}[t]
%\centering
%    \subfigure[Google App Indexing]{
%    \label{fig:appindexing} %% label for first subfigure
%    \includegraphics[width=0.45\columnwidth]{figures/google}}
%    \subfigure[Facebook App Links]{
%    \label{fig:applinks} %% label for second subfigure
%    \includegraphics[width=0.5\columnwidth]{figures/facebook}}
%  %\includegraphics[width=0.8\columnwidth]{figures/google}
%  \caption{Usage scenarios of deep links.}~\label{fig:example}
%\end{figure}
