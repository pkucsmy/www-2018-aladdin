Essentially, reaching an activity with a deep link is to issue one intent that could launch the target activity. However, the complexity of activity communications in Android apps makes it not easy to use a single intent to reach an activity for existing apps. For example, a target activity may rely on internal objects that are created by previous activities in the path from the main activity to the target activity. To address the issue, instead of a single intent to an activity, Paladin abstracts a deep link to an activity as a sequential path of activity transitions via intents, starting from the main activity of the app to the target activity pointed by the deep link. Therefore, the activity dependency can be resolved because we actually follow the original logic of activity communications.

\subsubsection{Navigation Analysis}

Since activities are loosely coupled that are communicated through intents, there is no explicit control flow between activities. Therefore, we design a \textit{Navigation Graph} to abstract the activity transitions. Here we give the formal definitions of activity transition and navigation graph.
\noindent \begin{Definition}[Activity Transition]
An activity transition $t(\mathcal{L})$ is triggered by an intent, where $\mathcal{L}$ is the combination of all the fields of the intent including action, category, data, and objects of basic types from the extra field.
\end{Definition}

Since an intent essentially encapsulates several messages passed between two activities, we use a label set $\mathcal{L}$ to abstract an intent. Two intents are equivalent if and only if the label sets are completely the same. Note that from the extra field, which is the major place to encapsulate messages, we take into account only the objects of basic types including \texttt{int}, \texttt{double}, \texttt{String}, etc. The reason is that objects of app-specific classes are usually internally created but cannot be populated from outside of the app. As a result, this kind of intents cannot be replayed at runtime.
\begin{Definition}[Navigation Graph]
A Navigation Graph $G$ is a directed graph with a start vertex. It is denoted as a 3-tuple, $G<V,E,r>$, where $V$ is the set of vertices, representing all the activities of an app; $E$ is the set of directed edges, and every single $e(v_1,v_2)$ represents an activity transition $t(\mathcal{L})$; $r$ is the start vertex.
\end{Definition}

In such a navigation graph, the start vertex $r$ refers to the main activity of the app. We assume that each node in $V$ could be reachable from the start vertex $r$. The navigation graph can have multi-edges, i.e., $\exists e_1,e_2\in E, v_{start}(e_1)=v_{start}(e_2)~and~v_{end}(e_1)=v_{end}(e_2)$, indicating that there can be more than one transition between two activities. In addition, it should be noted that the navigation graph can be cyclic.

\subsubsection{Shortcut Analysis}
After constructing the navigation graph, we analyze the paths to each activity.
\begin{Definition}[Path]
A path to an activity $\mathcal{P}_a$ is an ordered list of activity transitions $\{t_1, t_2, \dots, t_k\}$ starting from the main activity, where $k$ is the length of the path.
\end{Definition}

According to the path definition, the activity transition $t_1$ is always the app-launching intent that opens the main activity. The path $\mathcal{P}_a$ can ensure that all the internal dependencies are properly initialized before reaching the activity $a$.

In practice, there can be various paths to reach a specific activity. Since our approach uses the activity transitions to reach activities by deep links, the path should be as short as possible to reduce the execution time at the system level. Therefore, for each activity, we compute the shortest path, denoted as the shortcut, and the combination of labels in the activity transitions of the path constitutes the label set of the shortcut.
\begin{Definition}[Shortcut]
A Shortcut $\mathcal{T}(\mathcal{L})$ of an activity $a$ is the shortest path $\mathcal{P}_a = \{t_i\}$, and $\mathcal{L} = \cup \mathcal{L}{t_i}$.
\end{Definition}


%Figure~\ref{fig:wallstreet} shows the example in the ``\textit{Wallstreet News}'' app. If we want to reach a news page (\texttt{NewsDetailActivity}), there are two paths. One is to navigate directly from the \texttt{MainActivity}. The other is to switch to the topic page of \texttt{NewsTopicActivity} and then navigate to the news page. Obviously, the former path is shorter than the latter one. Since our approach uses the activity transitions to reach activities by deep links, the path should be as short as possible to reduce the execution time at system level. However, in some circumstances, a shorter path to an activity cannot cover all the instances of the activity. For example, the intermediate activities on the path can depend on some internal variables, so that they can be reached only via a longer path where these variables are assigned.
%
%\begin{figure}[t]
%\centering
%  \includegraphics[width=0.6\columnwidth]{figures/wallstreet}
%  \caption{Example of Wallstreet News App.}~\label{fig:wallstreet}
%\end{figure}
%
%We define that the path $p_1$ can replace the path $p_2$ if and only if $\mathcal{L}_{p_1}\subset \mathcal{L}_{p_2}$. Here $\mathcal{L}_{p_j}$ is the combination of all the labels in the path $p_j$: $\mathcal{L}_{p_j}=\cup\{\mathcal{L}(t_i)|t_i\in p_j\}$. For example, in the ``\textit{Wallstreet News}'' app, the path 1 from the \texttt{MainActivity} to the \texttt{NewsDetailActivity} has three labels, i.e., ``\textit{nid}'', ``\textit{image\_url}'', and ``\textit{news\_type}''. We can observe that all these three labels are included in path 2 which passes the \texttt{NewsTopicActivity}. Hence, we can use path 1 to replace path 2.
%
%With the definition of path replacement, we can define the possible shortest path as a Shortcut. We can use the shortcut to replace the path to an activity accounting for shorter execution time.
%\begin{Definition}[Shortcut]
%A Shortcut of a path $\mathcal{T}(p)$ is the shortest path that can replace path $p$.
%\end{Definition}
%
%We design the Algorithm~\ref{algo:shortcut} for finding the shortcuts in navigation graph $G$. For every vertex in the graph, we get its paths (Line 2) and sort the paths by their length in ascending order (Line 3). Then we enumerate every path in the path list, to find the shortest path that can replace it (Line 4-12), if they fulfill the requirement of label containing (Line 7). Due to the increasing sequence, the first available path that we obtain is the shortest one. We store it in a two-dimension map structure (Line 8).
%\begin{algorithm}
%\small
%    \caption{Shortcut computation.}
%    \label{algo:shortcut}
%    \SetAlgoLined
%    \KwIn{Navigation Graph $G<V,E,r>$ }
%    \KwOut{Shortcut $\mathcal{C}$}
%        \ForEach{$v\in V$} {
%            $Plist\leftarrow G.path(v)$;\\
%            sort\_by\_length($Plist$);\\
%            \For{$i\leftarrow 1~to~Plist.length$} {
%                $Shorcut[<v, p_i>]\leftarrow p_i$;\\
%                \For{$j\leftarrow 1~to~i-1$} {
%                    \If{$\mathcal{L}_{p_j}\subset\mathcal{L}_{p_i}$} {
%                        $Shorcut[<v, p_i>]\leftarrow p_j$;\\
%                        break;
%                    }
%                }
%            }
%        }
%\end{algorithm}
%
%After computing the shortcuts to an activity, we filter out the unique shortcuts and then put all of them into the intent sequence in the deep link template corresponding to the activity. The labels in the intents can be configured by developers as parameters. At runtime, these parameters are assigned values to reconstruct the shortcut.
